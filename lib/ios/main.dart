import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../load_csv/data_helper.dart';
import '../load_csv/load_data.dart';

class IosMain extends StatelessWidget {

  IosMain({Key? key}) : super(key: key);
  DataHelper dataHelper = DataHelper();

  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      debugShowCheckedModeBanner:false,home: HomePage( title:"Hope speaks",dataHelper:dataHelper),
   /* theme:  CupertinoThemeData(
        brightness: Brightness.dark,
        primaryColor: CupertinoColors.activeGreen)*/
      );
  }
}
class HomePage extends StatelessWidget {
  DataHelper dataHelper;
  String title;
  HomePage({Key? key, required this.dataHelper, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold( navigationBar: CupertinoNavigationBar(leading: Text(title),),
      child:  Connection(dataHelper:dataHelper)
    );
  }
}

