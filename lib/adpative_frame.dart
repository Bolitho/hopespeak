import 'package:flutter/material.dart';
import 'package:hopespeak/anriod/main.dart';
import 'package:hopespeak/ios/main.dart';
class AdpativeFramWork extends StatelessWidget {
  const AdpativeFramWork({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var platform = Theme.of(context).platform == TargetPlatform.iOS;
    return platform? IosMain(): MainAndroid();;
  }
}
