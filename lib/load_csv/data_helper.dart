import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:hopespeak/load_csv/read_csv.dart';
import 'package:translator/translator.dart';


class DataHelper {
  DataHelper();

  static DataHelper dataHelper = DataHelper();
  List<Main> dataImageNames = [];
  List<Main> luganda =[];

  Future<String> get _loadFileFromAssets async {
    final data = await rootBundle.loadString("assets/words.csv");
if (kDebugMode) {

}
    return data;
  }
  Future<String> get _loadFileFromAssetsLug async {
    final data = await rootBundle.loadString("assets/luganda_words.csv");
    if (kDebugMode) {

    }
    return data;
  }
  Future<List<String>> get _splitString async {

    final data = await _loadFileFromAssets;
    var dataList = data.split("|");

 var dataCleaned= dataList.where((element) =>  element != "").toList();



    if (kDebugMode) {
      print("clean data ${dataCleaned}");
    }
    return dataCleaned;
  }
  Future<List<String>> get _splitLugString async {

    final data = await _loadFileFromAssetsLug;
    var dataList = data.split("|");

    var dataCleaned= dataList.where((element) =>  element != "").toList();



    if (kDebugMode) {
      print("clean data ${dataCleaned}");
    }
    return dataCleaned;
  }


  Future<List<Main>> readCsv() async {

    try {
      var content = await _splitString;




      // Read the file

      for (var i = 0; i < content.length; i++) {

        var singleItem = content[i].split("&");


print( "the value of i: $i");
var length = content.length;
print("length:$length");
        if(i == length-2){
          dataImageNames.add(Main(
              imageName: singleItem[0], fileName: int.parse(singleItem[1])));
          print(singleItem);

          break;
        }
        else{


          dataImageNames.add(Main(
              imageName: singleItem[0].trim(), fileName: int.parse(singleItem[1])));
        }


      }

    } catch (e) {
      // If encountering an error, return 0
      if (kDebugMode) {
        print("there was an error when reading the csv data${e}");
      }
    }
    return dataImageNames;
  }
  Future<List<Main>> convertToLuganda() async {


    try {
      var content = await _splitLugString;

      for (var i = 0; i < content.length; i++) {

        var singleItem = content[i].split("&");
        var length = content.length;

        if(i == length-1){

            luganda.add(Main(imageName:  singleItem[0].trim(), fileName: int.parse(singleItem[1].trim() )  ));

          if (kDebugMode) {
            print(singleItem);
          }

          break;
        }
        else{


            luganda.add(Main(imageName:  singleItem[0].trim(), fileName: int.parse(singleItem[1].trim() )  ));

        }


      }

    } catch (e) {
      // If encountering an error, return 0
      if (kDebugMode) {
        print("there was an error when reading the csv data${e}");
      }
    }


    return luganda;

  }
}
