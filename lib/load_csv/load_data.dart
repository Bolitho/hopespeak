import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:hopespeak/load_csv/data_helper.dart';
import 'package:hopespeak/load_csv/read_csv.dart';
import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:flutter/cupertino.dart';

class Connection extends StatefulWidget {
  DataHelper dataHelper;

  Connection({Key? key, required this.dataHelper}) : super(key: key);

  @override
  State<Connection> createState() => _ConnectionState();
}

class _ConnectionState extends State<Connection> {

  List<Main> english = [];
  List<Main> luganda = [];

  bool isPlaying = false;
  final assetsAudioPlayer = AssetsAudioPlayer();
  FlutterTts flutterTts = FlutterTts();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    start();
    ttsSettings();
  }


  bool isSpeaking = true;
  bool whichLanguage = false;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery
        .of(context)
        .size
        .width;
    bool isWidth = width <= 320;

    double fonts = isWidth ? 15.0 : 20.0;
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Switch to english"),
              Switch.adaptive(value: whichLanguage, onChanged: (value) {
                setState(() {
                  whichLanguage = value;
                });
              }),
            ],
          ),
          Flexible(
            child: whichLanguage ? gridView(english, isWidth) : gridView(
                luganda, isWidth),
          ),
        ],
      ),
    );
  }

  gridView(List<Main> list, widthBool) {
    return GridView.builder(
      itemCount: whichLanguage ? english.length : luganda.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: widthBool ? 3 : 4,

      ),
      itemBuilder: (BuildContext context, index) {
        return
          Stack(
              children: [

          Container( child:
          GestureDetector(

          onTap: whichLanguage? ()
        =>
            speaking(translation: "${list[index].imageName}")
        :()=>speaking( translation:"assets/luganda_audios/${list[index].fileName}.mp3"),

        child: Image.asset("assets/core_word_communication_board/${ list[index].fileName}.png",)
        )
        ),
        Align( alignment: Alignment.topCenter,
        child: FittedBox(child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FittedBox(child: Text( list[index].imageName ,style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),textAlign: TextAlign.center,)),
        )
        )
        ,
        )
        ]
        );


      },


    );
  }

  Future<void> start() async {
    await widget.dataHelper.convertToLuganda().then((value) {
      setState(() {
        luganda = value;
        widget.dataHelper.readCsv().then((values) {
          setState(() {
            english = values;
          });
        });
      });
    });
  }

  Future<void> stop() async {
    await flutterTts.stop();
  }

  Future<void> speak(String text) async {
    await flutterTts.speak(text);
  }

  void ttsSettings() {
    flutterTts.setSpeechRate(0.5);
  }

  Future<void> speaking({required String translation}) async {
    if(whichLanguage){
    speak(translation);
    return;
    }
    await assetsAudioPlayer.open(Audio(translation));
    isSpeaking = !isSpeaking;
    isSpeaking ? await assetsAudioPlayer.stop() : await assetsAudioPlayer
        .play();
  }


}

