import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../learning_area.dart';
import '../load_csv/data_helper.dart';
import '../load_csv/load_data.dart';
import '../load_csv/read_csv.dart';


class MainAndroid extends StatelessWidget {
   MainAndroid({Key? key}) : super(key: key);
DataHelper dataHelper = DataHelper();

  @override
  Widget build(BuildContext context) {
    print("");
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Hope speaks',
      theme: ThemeData(

        primarySwatch: Colors.green,
      ),
      home:  HomePage(title: "Hope speaks",dataHelper:dataHelper,),
    );
  }
}
  class HomePage extends StatelessWidget {

    DataHelper dataHelper;
    String title;


     HomePage({Key? key, required this.title, required this.dataHelper}) : super(key: key);

    @override
    Widget build(BuildContext context) {

      var appBar=  AppBar();
      var height = MediaQuery.of(context).size.height-appBar.preferredSize.height - MediaQuery.of(context).padding.top;
      var width = MediaQuery.of(context).size.width;
      if (kDebugMode) {
        print("width:$width");
      }
      if (kDebugMode) {
        print( "height: $height");
      }

      return Scaffold( appBar:AppBar(title: Text(title),),body:Connection(dataHelper:dataHelper)

      );

    }
}


