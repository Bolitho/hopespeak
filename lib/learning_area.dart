import 'package:flutter/material.dart';
import 'package:hopespeak/load_csv/read_csv.dart';

import 'load_csv/data_helper.dart';
class CustomizeStack extends StatefulWidget {
  DataHelper dataHelper;
   CustomizeStack({Key? key,required this.dataHelper}) : super(key: key);

  @override
  State<CustomizeStack> createState() => _CustomizeStackState();
}

class _CustomizeStackState extends State<CustomizeStack> {
  List<Main> names =[];
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.dataHelper.readCsv().then((value){
      setState(() {
        names = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {


    return  Scaffold(
      appBar: AppBar(),
     body : Padding(
        padding: const EdgeInsets.all(8.0),
        child: GridView.builder(

            itemCount: names.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount:  4),
        itemBuilder: (BuildContext context, index){
        return Stack(
          children: [Image.asset("assets/core_word_communication_board/${index+1}.png"), Align(  alignment:Alignment.topCenter,child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: FittedBox(child: Text(" ${names[index].imageName}",style: TextStyle(fontSize: 20.0),)),
          ))],);

        },),
      ),
    );

  }
}
